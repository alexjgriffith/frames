love.conf = function(t)
   t.gammacorrect = true
   t.title, t.identity = "Map Maker", "Minimal"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 7 * 120
   t.window.height = 20 * 30
   t.window.vsync = true
   t.version = "11.2"
end
