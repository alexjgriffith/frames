(local array2d {})
(local vector (require "vector"))

(tset array2d :sample-data [0 0 0 1 1 1 1 0])

(fn array2d.angle_to_matrix [theta]
  [(math.cos theta) (- (math.sin theta))
   (math.sin theta) (math.cos theta)])

(fn array2d.rotate [points matrix]
  (array2d.mappend points
               (fn [point] (vector.mult-m point matrix))))

(fn array2d.scale [points scale]
  (array2d.mappend points
               (fn [point] (vector.mult-s2 point scale))))

(fn array2d.move [points move]
  (array2d.mappend points
               (fn [point] (vector.add point move))))


(fn array2d.reverse [tab]
  (local len (# tab))
  (local ret [])
  (for [i len 2 2]
    (table.insert ret (. tab (- i 1)))
    (table.insert ret (. tab i) ret))
  ret)

(fn array2d.mappend [tab function args]
  (local len (# tab))
  (local ret [])
  (for [i 1 len 2]
    (let [[x y] (function [(. tab i) (. tab (+ i 1))] args)]
      (table.insert ret x)
      (table.insert ret y)))
  ret)

(fn array2d.map [tab function args]
  (local len (# tab))
  (local ret [])
  (for [i 1 len 2]
      (table.insert ret (function [(. tab i) (. tab (+ i 1))] args)))
  ret)

(fn array2d.sweep [tab num function args]
  (local len (# tab))
  (local ret [])
  (for [i 1 (- len (* 2 (- num 1))) 2]
    (let [points []]
      (for [j i (+ i (* num 2))]
        (table.insert points (. tab j)))
      (table.insert ret (function points args))))
  ret)

(fn array2d.sweep-i [tab num function args]
  (local len (# tab))
  (local ret [])
  (for [i 1 (- len (* 2 (- num 1))) 2]
    (let [points []]
      (for [j i (+ i (* num 2))]
        (table.insert points  j))
      (table.insert ret (function points args))))
  ret)

(fn array2d.circular-sweep [tab num function args]
  (local len (# tab))
  (local ret [])
  (for [i 1 len 2]
    (let [points []]
      (for [j i (+ i (* num 2))]
        (if (< j len)
            (table.insert points (. tab j))
            (table.insert points (. tab (- j len)))))
      (table.insert ret (function points args))))
  ret)

(fn array2d.circular-sweep-i [tab num function args]
  (local len (# tab))
  (local ret [])
  (for [i 1 len 2]
    (let [points []]
      (for [j i (+ i (* num 2))]
        (if (< j len)
            (table.insert points  j)
            (table.insert points (- j len))))
      (table.insert ret (function points args))))
  ret)

(assert (array2d.circular-sweep-i array2d.sample-data 2 (fn [[x1 y1 x2 y2]] [[x1 y1] [x2 y2]])))

array2d
