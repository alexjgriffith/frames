(local colour {})

(fn max [t]
    (var ret (. t 1))
    (for [i 2 (# t)]
         (let [v (. t i)]
           (when (> v ret)
             (set ret v))))
    ret)

(fn min [t]
    (var ret (. t 1))
    (for [i 2 (# t)]
         (let [v (. t i)]
           (when (< v ret)
             (set ret v))))
    ret)


(fn colour.hsl-to-rgb [hin s l a]
    (var [r g b] [0 0 0])
    (local h (/ hin 360))
    (local hue2rgb (fn hue2rgb [p q tin]
                       (var t tin)
                       (when (< t  0) (set t (+ t 1)))
                       (when (> t  1) (set t (- t 1)))
                       (if
                        (< t  (/ 1 6)) (+ p (* t 6 (+ q (- p))))
                        (< t  (/ 1 2)) q
                        (< t  (/ 2 3)) (+ p (* (+ (/ 2 3) (- t)) 6 (+ q (- p))))
                        p)))
    (if (= s 0)
        (set [r g b] [l l l])
        (let [q (if (< l 0.5) (* l (+ 1 s)) (+ l s (- (* l s))))
              p (+ (* 2 l) (- q))]
          (set r  (hue2rgb p q (+ h (/ 1 3))))
          (set g  (hue2rgb p q h))
          (set b  (hue2rgb p q (+ h (- (/ 1 3)))))))
    [r g b a])

(fn colour.normalize [te]
    (let [ max (max te) min (min te)]
        (lume.map te (fn [e] (/ (+ e (- min)) (+ max (- min)))))))

(fn colour.sigmoid [x vmax xmid k]
    (/ vmax (+ 1 (math.exp (- (* k (+ x (- xmid))))))))

(fn colour.noise [width height off freq amp]
    (var value {})
    (var val 0)
    (for [i 1 width]
         (for [j 1 height]
              (set val 0)
              (for [k 1 6]
                   (set val (+ val (* (/ amp k) (love.math.noise
                                                 (+ off (* freq k (/ i width)))
                                                 (+ off (* freq k (/ j height))))))))
              (table.insert value val)))
    (colour.normalize value))

(fn int-clamp-0-255 [value]
  (math.min (math.max (math.floor (* 255 value)) 0) 255))

(fn colour.rgb-to-byte [[r g b a]]
  (string.char
   (int-clamp-0-255 r) (int-clamp-0-255 g) (int-clamp-0-255 b) (int-clamp-0-255 a)))

(fn colour.rgb-array-to-bytedata [array]
  (local rgb-to-byte colour.rgb-to-byte)
  (local tab [])
  (each [_ rgb (ipairs array)]
    (table.insert tab (rgb-to-byte rgb)))
  (table.concat tab ""))

(fn colour.hsl-array-to-bytedata [array]
  (local rgb-to-byte colour.rgb-to-byte)
  (local hsl-to-rgb colour.hsl-to-rgb)
  (local tab [])
  (each [_ rgb (ipairs array)]
    ;;(pp rgb)
    (table.insert tab (rgb-to-byte (hsl-to-rgb (unpack rgb)))))
  (table.concat tab ""))


colour
