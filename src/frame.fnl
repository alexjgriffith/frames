(local gamera (require :lib.gamera))

(local frame {})

(local element-types
       {:headbar {:pos (fn [parent params]
                         {:x parent.x :y parent.y :w parent.w :h 20})
                  :colour [1 1 1 1]
                  :lining [0 0 0 1]
                  :children {}
                  :render :rectangles}
        :footbar {:pos (fn [parent params]
                         {:x (+ parent.x 40) :y (+ parent.y parent.h -40)
                          :w (- parent.w 80) :h 40})
                  :colour [1 1 1 1]
                  :lining [0 0 0 1]
                  :children {}
                  :render :rectangles}
        :button {:pos (fn [parent params]
                        (local num params.num)
                        {:x (+ parent.x 5 (* num (+ 16 2)))
                         :y (+ parent.y 2)
                         :w 16
                         :h 16})
                 :colour [0 0 0 1]
                 :children {}
                 :render :rectangles
                        }})

(fn draw-element-rectangle [element]
  (let [{:x x :y y :w w :h h} element.pos]
    (love.graphics.setColor element.colour)
    (love.graphics.rectangle "fill" x y w h)
    (when element.lining
      (love.graphics.setColor element.lining)
      (love.graphics.rectangle "line" x y w h))))


(local element-renders
       {:rectangles draw-element-rectangle})

(local elements {})

(fn elements.new [parent element-type params?]
  (local element {})
  (each [key value (pairs (. element-types element-type))]
    (tset element key (if (= (type value) "function")
                          (value parent params?)
                          value)))
  (if params?
      (each [key value (pairs params?)]
      (tset element key (if (= (type value) "function")
                            (value parent params?)
                            value))))
    element)

(fn frame.new [x y w h]
  (let [camera (-> (gamera.new 0 0 (- w x) (- h y))
                   (gamera.setWindow x y w h))]
    {:pos {:x x :y y :w w :h h} :layer 0 :data {} :keybindings {} :camera camera
     :elements []}))

(fn frame.within [pos x y]
  (let [{:x fx :y fy :w fw :h fh} pos]
    (and (and (> x fx) (<= x (+ fx fw)))
         (and (> y fy) (<= y (+ fy fh))))))

(fn frame.draw [self fun]
  (local headbar (elements.new self.pos :headbar))
  (local button (elements.new headbar.pos :button {:num 0}))
  (local button2 (elements.new headbar.pos :button {:num 1}))
  (local footbar (elements.new self.pos :footbar))
  (local pos (fn [parent params]
               (local num params.num)
               {:x (+ parent.x 7 (* num (+ 30 4)))
                :y (+ parent.y 5)
                :w 30
                :h 30}))
  (local buttons
         (lume.map [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14]
                   (fn [n] (elements.new footbar.pos :button
                                         {:num n :pos pos}))))



  ;; Free render element
  (gamera.draw self.camera fun)
  (draw-element-rectangle headbar)
  (draw-element-rectangle button)
  (draw-element-rectangle button2)
  (draw-element-rectangle footbar)
  (lume.map buttons draw-element-rectangle)
  )


frame
