(local params (require :params))

(fn noise-index [i j width height off freq amp n]
  (var val 0)
  (var max 0)
  (for [k 1 n]
    (set max (+ max (/ amp k)))
    (set val (+ val (* (/ amp k) (love.math.noise
                                  (+ off (* freq k (/ i width)))
                                  (+ off (* freq k (/ j height))))))))
  (/ val max))

(fn straight-decline [[x y] w h]
  "Center at 0"
  (- 1 (/ (math.sqrt (+ (* x x) (* y y)))
     (math.sqrt (+ (* w w) (* h h))))))

(fn rotate [[x y] angle]
  (let [cosA (math.cos angle)
        sinA (math.sin angle)]
    [(- (* cosA x) (* sinA y))
     (+ (* sinA x) (* cosA y))]))

(fn move [[x y] l h]
  [(+ x l) (+ y h)])

(fn stretch [[x y] A B C D]
  [(if (> x 0 ) (* x A) (* x B))
   (if (> y 0 ) (* y C) (* y D))])

(fn noise [[x y] width height index frequency]
  (noise-index x y width height (* width index) frequency 1 6))

(fn calc-height [pos fallw fallh width height index frequency keys val]
  (* (noise pos width height index frequency)
                  (straight-decline pos fallw fallh)))

(fn calc-soil [pos width height index frequency keys val]
  (noise pos width height index frequency))

(fn calc-rainfall [pos width height index frequency]
  (noise pos width height index frequency))

(fn point-to-height [x y params]
  (-> [x y] (move params.ox params.oy)
      (rotate (* math.pi params.theta))
      (stretch (unpack params.stretch))
      (calc-height params.fallx params.fally params.width params.height params.index
                   params.frequency params.height-keys params.height-values)))

(fn point-to-soil [x y params]
  (-> [x y]
      (move params.ox params.oy)
      (calc-soil params.width params.height
                 params.soil-index params.soil-frequency params.soil-keys
                 params.soil-values)))

(fn point-to-rainfall [x y params]
  (-> [x y]
      (move params.ox params.oy)
      (calc-rainfall params.width params.height
                 params.rainfall-index params.rainfall-frequency)))

(fn biome [rainfall height params]
  (local moisture
         (if (> rainfall (. params.rainfall-keys 1)) :wet
             (> rainfall (. params.rainfall-keys 2)) :damp
              :dry))
  (local elevation
         (if (> height (. params.height-keys 1)) :mountain
             (> height (. params.height-keys 2)) :high-land
             (> height (. params.height-keys 3)) :low-land
             (> height (. params.height-keys 4)) :beach
             :water
             ))
  (local biome
         (match [elevation moisture]
           [:mountain _] :mountain
           [:high-land :wet] :dec-forest
           [:high-land :damp] :dec-forest
           [:high-land :dry] :tundra
           [:low-land :wet] :marsh
           [:low-land :damp] :con-forest
           [:low-land :dry] :prairie
           [:beach _] :beach
           [:water _] :water
           :water))
  biome)

(fn mode [tab]
  (local keys {})
  (var max-count nil)
  (var key nil)
  (each [_ val (ipairs tab)]
    (let [new-count (+ (or (. keys val) 0) 1)]
      (when (> new-count (or max-count 0))
        (set max-count new-count)
        (set key val))
      (tset keys val new-count)))
  key)

(fn select2d [[x y w h] width height tab]
  (local index (fn [i j] (+ 1 i (* j width))))
  (local col [])
  (for [i x (+ x w)]
    (for [j y (+ y h)]
      (table.insert col (. tab (index i j)))
      ))
  col)

(fn make-grid [params biomes]
  (let [width params.width
        height params.height
        w (math.ceil (/ width params.max-tile-w))
        h (math.ceil (/ height params.max-tile-h))
        tiles []]
    (for [i 0 (- params.max-tile-w 1)]
      (for [j 0 (- params.max-tile-h 1)]
        (table.insert
         tiles
         {:i i :j j :x (* i w) :y (* j h) :w w :h h
         :biome (-> [(* i w) (* j h) w h] (select2d width height biomes) (mode))})
        ))
    tiles))

(fn make-map [params]
  (local [width height] [params.width params.height])
  (local heights [])
  (local rainfall [])
  ;; (local tempurature [])
  (local soil [])
  ;; (local minerals [])
  (for [i 1 width]
    (for [j 1 height]
      (local [x y] [(- i (/ width 2)) (- j (/ height 2))])
      (local h (point-to-height x y params))
      (local s (point-to-soil x y params))
      (local r (point-to-rainfall x y params))
      ;; (local t (point-to-temperature x y params))
      (table.insert rainfall r)
      (table.insert heights h)
      (table.insert soil s)))
  (local tab3 [])
  (each [i val (ipairs  heights)]
    (table.insert tab3 (biome (. rainfall i) val params)))
  (local grid (make-grid params tab3))
  {:soil soil :heights heights :biomes tab3 :grid grid
   :width (math.ceil (/ params.width params.max-tile-w))
   :height (math.ceil (/ params.height params.max-tile-h)) })

make-map
