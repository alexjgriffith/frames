(local colour (require :colour))
(local make-map (require :make-map))

(local gamera (require :lib.gamera))

(local data (require :data))
(local params (require :params))

(local mini (require :mode-mini))
(local sq (require :mode-square))

(var image nil)
(var map nil)

(fn activate []
  (set map (make-map params))
  (sq.activate)
  (mini.activate)
  (local imgData
         (love.image.newImageData
          params.width params.height
          :rgba8
          (colour.hsl-array-to-bytedata
           (lume.map map.biomes (fn [biome]
                                  (. params.biome-colour biome))))))

  (set image (love.graphics.newImage imgData))
  (image:setFilter "nearest" "nearest"))

(fn incf [table key by min max]
  (let [by (or by 1)]
    (tset table key (math.min max (math.max min  (+ (. table key) by))))))

(fn decf [table key by]
  (let [by (or by 1)]
    (tset table key (- (. table key) by))))


(local sqrt2 (math.sqrt 2))

(activate)

(local toggle-table {})
(fn toggle [bool variable]
  (when (= nil (. toggle-table variable))
    (tset toggle-table variable false))
  (var ret false)
  (match [bool (. toggle-table variable)]
    [true false] (do (tset toggle-table variable true)
                   (set ret true))
    [true true] (tset toggle-table variable true)
    [false true] (tset toggle-table variable false)
    [false false] (tset toggle-table variable false))
  ret)

{:draw (fn draw [message]
         (sq.draw map)
         (mini.draw image))
 :update (fn update [dt set-mode]
           (let [y (+ (if (love.keyboard.isDown "down") 1 0)
                      (if (love.keyboard.isDown "up") -1 0))
                 x (+ (if (love.keyboard.isDown "left") -1 0)
                      (if (love.keyboard.isDown "right") 1 0))]
             ;; (pp [x y])
             (match [x y]
               ;; [0 0] (do :nothing)
               [_ 0] (incf data :x (* x dt data.speed) 0 2000)
               [0 _] (incf data :y (* y dt data.speed) 0 2000)
               [_ _] (do (incf data :y (* (/ y sqrt2) dt data.speed) 0 2000)
                       (incf data :x (* (/ x sqrt2) dt data.speed) 0 2000)))
             )
           ;; (pp toggle-table)
           (let [[x y] [(love.mouse.getPosition)]
                 click (toggle (love.mouse.isDown 1) :click)]
             (sq.update dt map x y click))

           (mini.update dt))

 :activate activate
 :wheelmoved (fn [x y]
               (love.mouse.getPosition)
               (pp [x y])
               ;; iterate through all cams? Maybe call them frames ;)
               )
 :keypressed (fn keypressed [key set-mode]
               ;;(love.event.quit)
               )}
