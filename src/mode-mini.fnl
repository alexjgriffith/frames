(local mini {})

(local colour (require :colour))

(local gamera (require :lib.gamera))

(local data (require :data))
(local params (require :params))

(fn mini.activate []
  (tset data :mini-cam
        (-> (gamera.new 0 0 2000 2000)
            (gamera.setWindow 0 0 params.min-width params.min-height))))

(fn mini.draw [image]
 (gamera.draw
  data.mini-cam
  (fn [l w h t]
    (love.graphics.setColor (colour.hsl-to-rgb (unpack (. params.biome-colour :water))))
    (love.graphics.rectangle "fill" 0 0 240 240)
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.draw image)
    (love.graphics.setColor (colour.hsl-to-rgb 0 0.5 0.5 1))
    (love.graphics.rectangle "line"  data.x data.y  150 150))))

(fn mini.update [dt]
  (gamera.setScale data.mini-cam (/ 2 5)))

mini
