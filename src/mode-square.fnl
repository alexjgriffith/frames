(local sq {})

(local colour (require :colour))

(local frame (require :frame))

(local gamera (require :lib.gamera))

(local data (require :data))
(local params (require :params))

(local hover {:x 0 :y 0})

(fn sq.activate []
  ;; (tset data :cam (-> (gamera.new 0 0 2000 2000)
  ;;                     (gamera.setWindow params.min-width 0
  ;;                                       (+ params.min-width params.width)
  ;;                                       params.height)))
  (let [x params.min-width
        y 0
        w params.width
        h params.height]
    (tset data :cam (frame.new  x y w h))))

(fn sq.draw [map]
  (frame.draw
   data.cam
   (fn [l w h t]
     (love.graphics.setColor (colour.hsl-to-rgb (unpack (. params.biome-colour :water))))
     (love.graphics.rectangle "fill" 0 0 2000 2000)
   (love.graphics.setColor 1 1 1 1)

   (each [_ {:biome biome
             :x x
             :y y
             :w w
             :h h} (ipairs map.grid)]
     (when (~= biome :water)
       (love.graphics.setColor (colour.hsl-to-rgb (unpack (. params.biome-colour biome))))
       (love.graphics.rectangle "fill" x y w h)
       (love.graphics.setColor 1 1 1 0.1)
       (love.graphics.rectangle "line" x y w h)))
   (love.graphics.setColor 1 1 0 1)
   (love.graphics.rectangle "line" (* 10 hover.x) (* 10 hover.y) 10 10)
   )))

(fn sq.update [dt map x y click]
  (local cam data.cam.camera)
  (let [[x y] [(gamera.toWorld cam x y)]]
    (tset hover :x (math.floor (/ x 10)))
    (tset hover :y (math.floor (/ y 10))))
  (when click
    ;; (pp map.grid)
    (pp map.width)
    ;; (pp (+ hover.x 1 (* hover.y map.width)))
    (pp [hover.x hover.y])
    (pp (. map.grid (+ hover.y 1 (* hover.x (/ 600 map.width))))))
  (gamera.setPosition cam
                      (+ data.x (/ cam.w2 4))
                      (+ data.y (/ cam.h2 4)))
  (gamera.setPosition data.mini-cam data.mini-cam.w2 data.mini-cam.h2)
  (gamera.setScale cam 4))

sq
