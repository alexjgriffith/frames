{:ox 170
 :oy -20
 :theta 0.2
 :stretch [0.4 2 0.6 2]
 :fallx 160
 :fally 160
 :width (* 5 120)
 :height (* 5 120)
 :min-height (* 2 120)
 :min-width (* 2 120)
 :sub-height (* 3 120)
 :sub-width (* 2 120)
 :index 94
 :frequency 3
 :height-keys [0.5 0.4 0.10 0.075]
 :height-values [1 0.8 0.5 0.2 0]
 :soil-frequency 3
 :soil-index 1
 :rainfall-keys [0.7 0.5]
 :soil-keys [0.5 0.3]
 :soil-values [300 30 60]
 :rainfall-index 91
 :rainfall-frequency 2
 :seed 100
 :max-tile-w 60
 :max-tile-h 60
 :biome-colour {:mountain [0 1 1 1]
                :dec-forest [120 0.4 0.6 1]
                :tundra [240 0.1 0.7 1]
                :marsh [160 0.7 0.5 1]
                :con-forest [120 0.7 0.5 1]
                :prairie [39 0.77 0.83 1]
                :beach [45 0.36 0.46 1]
                :water [240 0.7 0.2 1] ;; [0 1 1 1];;
                }}
