(var once-flag false)
(fn once [fun]
  (when (not once-flag)
    (set once-flag true)
    (fun)))
