;;; Based on https://github.com/wkwan/2d-vector-math/blob/master/vector.lua
;;; To add:
;;; - (fn mult-v [vect1 vect2]) -> vect
;;; - (fn sub-v [vect1 vect2 vect3 vect4]) -> [vect vect]
;;; - (fn add-v [vect1 vect2 vect3 vect4]) -> [vect vect]
;;; - (fn orth [vect1 vect2 float]) -> vect
;;; - (fn half [vect1 vect2]) -> vect

(local vector {})

;;; RETURNS SCALAR
(fn vector.is-zero? [[x y]] (and (= x 0) (= y 0)))

(fn vector.len-sq [[x y]] (+ (^ x 2) (^ y 2)))

(fn vector.len [[x y]] (math.sqrt (+ (^ x 2) (^ y 2))))

;; positive: same direction
;; negative: different direction
;; zero: perpendicular
(fn vector.dot [[x y] [ox oy]] (+ (* x ox) (* y oy)))

;; positive: otherVec is CW of vector
;; negative: otherVec is CCW of vector
;; zero: parallel
(fn vector.cross [[x y] [ox oy]] (- (* x oy) (* y ox)))

(fn vector.distance-sq [[x y] [ox oy]] (+ (^ (- ox x) 2) (^ (- oy y) 2)))

(fn vector.distance [[x y] [ox oy]]
  (math.sqrt (+ (^ (- ox x) 2) (^ (- oy y) 2))))

(fn vector.equals [[x y] [ox oy]] (and (= x ox) (= y oy)))

(fn vector.print [[x y]] (print (.. "x: "  x  ", y:"  y)))

;;; RETURNS VECTOR
(fn vector.perp-ccw [[x y]] [(- y) x])

(fn vector.perp-cw [[x y]] [y (- x)])

(fn vector.reverse [[x y]] [(- x) (- y)])

(fn vector.add [[x y] [ox oy]] [(+ x ox) (+ y oy)])

(fn vector.sub [[x y] [ox oy]] [(- x ox) (- y [ox oy])y])

(fn vector.mult-s [[x y] scalar] [(* x scalar) (* y scalar)])

(fn vector.mult-s2 [[x y] [ox oy]] [(* x ox) (* y oy)])

(fn vector.mult-m [[x y] [ox1 oy1 ox2 oy2]]
  [(+ (* x ox1) (* y oy1)) (+ (* x ox2) (* y oy2))])

(fn vector.div-s [[x y] scalar] [(/ x scalar) (/ y scalar)])

(fn vector.norm [vect] (vector.div vect (vector.len vect)))

(fn vector.scale [vect mag] (-> vect (vector.norm) (vector.mult-s mag)))

;;; MODIFIES ORIGINAL VECTOR (REMOVED)

vector
